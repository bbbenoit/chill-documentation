.. Copyright (C)  2014 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".

.. _install-production-webserver:

Install production webserver
############################

.. todo::

   the section "Install production webserver" must be written. Help appreciated :-)

.. warning::

   Some sensitive data (like the person data, ...) might be logged in a special channel, called ``chill``. 

   This channel will log events like items removed by a user, what where the details of this item, who removed it, ...

   You should take care of encrypting or discarding those data if required. 

   For an how-to of how to encrypt those data, you may consult `the appropriate section of the symfony documentation <http://symfony.com/doc/current/cookbook/logging/monolog.html#handlers-and-channels-writing-logs-to-different-locations>`_
   
