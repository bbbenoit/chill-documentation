.. Copyright (C)  2016 Champs Libres Cooperative SCRLFS
   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation;
   with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
   A copy of the license is included in the section entitled "GNU
   Free Documentation License".


Javascript functions
####################

Some function may be useful to manipulate elements on the page.

Show-hide elements according to checkbox
****************************************

The function ``chill.listenerDisplayCheckbox`` will make appears / disappears elements according to a checkbox (if the checkbox is checked, the elements will appears).

Usage
=====

The checkbox must have the data `data-display-target` with an id, and the parts to show/hide must have the data `data-display-show-hide` with the same value.

On the same page, you should run the function ``chill.listenerDisplayCheckbox``.

Example : 

.. code-block:: html

     <input data-display-target="export_abc" value="1" type="checkbox">
     
     <div data-display-show-hide="export_abc">
     <!-- your content here will be hidden / shown according to checked state -->
     </div>
     
     <!-- you could use the block "js" to render this script at the bottom of the page -->
     <!-- {{ block js }} -->
     <script type="text/javascript">
        window.addEventListener("DOMContentLoaded", chill.listenerDisplayCheckbox);   
     </script>
     <!-- {{ endblock js }} -->

.. note:: Hint
     
     For forms in symfony, you could use the `id` of the form element, accessible through `{{ form.vars.id }}`. This id should be unique.

