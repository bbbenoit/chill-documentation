



Useful snippets
###############


Security
********

Get the circles a user can reach
================================

.. code-block:: php

   use Symfony\Component\Security\Core\Role\Role;

   $authorizationHelper = $this->get('chill.main.security.authorization.helper');
   $circles = $authorizationHelper
       ->getReachableCircles(
             $this->getUser(), # from a controller
             new Role('CHILL_ROLE'), 
             $center 
       );


Controller
**********

Secured controller for person
=============================

.. literalinclude:: useful-snippets/controller-secured-for-person.php
   :language: php
